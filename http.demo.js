// index.js
// Nodejs now includes an experimental dev-tools debugger
// run command: node --inspect index.js

const http = require('http');
const port = 3000;

// Parameter for constructor of server.
// This function is automatically added
// to the 'request' event which is emitted
// each time there is a request.
const requestHandler = (request, response) => {
    console.log(request.url);

    // The end function call finishes sending
    // the request.
    response.end("Hello Node.js Server!");
}

const server = http.createServer(requestHandler)

// The listen function call will make the
// server start accepting connections on the
// specified port.
server.listen(port, (err) => {
    if (err) {
        return console.log("ERROR HAPPENED: ", err)
    }

    console.log(`Server is listening on port ${port}`);
})


